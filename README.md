# LaTeX Custom Debug

Some simple custom debug packages for LaTeX documents.

# Commands

1. **debug.sty**:
  - `\alert{title}{body}`: a coloured box showing a big `title` and a `body` on a red background;
2. **holomorphic.sty**:
    1. _generic commands_:
        - `\overbar`: new typeset bar over letters (longer than `\bar`, shorter than `\overline`),
        - `\cpx{CMD}`: puts `\overbar` over `CMD`;
    2. _derivatives_:
      - `\bpartial`: `\overbar` over the `\partial` sign;
    3. _variables_:
      - `\bz`: `\overbar` over variable `z`,
      - `\bu`: `\overbar` over variable `u`,
      - `\bt`: `\overbar` over variable `t`;
    4. _bosonic fields_:
      - `\bX`: `\overbar` over field `X`,
      - `\lX`: left moving field `X`,
      - `\rX`: right moving field `X`,
      - `\bphi`: `\overbar` over field `\phi`,
      - `\lphi`: left moving field `\phi`,
      - `\rphi`: right moving field `\phi`,
      - `\bPhi`: `\overbar` over field `\Phi`,
      - `\lPhi`: left moving field `\Phi`,
      - `\rPhi`: right moving field `\Phi`,
      - `\bT`: `\overbar` over field `T` (stress energy tensor);
    5. _fermionic fields_:
      - `\bpsi`: `\overbar` over field `\psi`,
      - `\lpsi`: left moving field `\psi`,
      - `\rpsi`: right moving field `\psi`,
      - `\bPsi`: `\overbar` over field `\Psi`;
      - `\lPsi`: left moving field `\Psi`;
      - `\rPsi`: right moving field `\Psi`;
    6. _double fields_:
      - `\dX`: equivalent to the double field `\mathcal{X}`,
      - `\bdX`: `\overbar` over field `\dX`.

# License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.