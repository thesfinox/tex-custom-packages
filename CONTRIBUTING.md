# Contribution guidelines

You are free to add, modify, delete pieces of code as long as you can provide a
good explanation about your choice.